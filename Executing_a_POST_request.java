
// To use these Internet methods, AndroidManifest.xml must have the following permission:
//<uses-permission android:name="android.permission.INTERNET"/>
// Create the Apache HTTP client and post  
 HttpClient httpclient = new DefaultHttpClient();
 HttpPost httppost = new HttpPost("http://www.website.org/service.php");  
   
     try {  
         // Add data to your post  
     List<NameValuePair> pairs = new ArrayList<NameValuePair>(2);  
     pairs.add(new BasicNameValuePair("ID", "VALUE"));  
     pairs.add(new BasicNameValuePair("string", "Yeah!"));  
     httppost.setEntity(new UrlEncodedFormEntity(pairs));  
   
     //Finally, execute the request  
     HttpResponse webServerAnswer = httpclient.execute(httppost);  
       
 } catch (ClientProtocolException e) {  
     //Deal with it  
 } catch (IOException e) {  
     //Deal with it  
 }	